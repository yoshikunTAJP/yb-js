class YukiBBS {
    constructor(name, seed) {
        this.name = name;
        this.seed = seed;
        this.msg = document.getElementById("result");
        this.msgN = this.msg.children[1].children[0].children[1].children[0].innerText;
    }

    send(message, channel) {
        sendMessage(this.name, this.seed, channel, message);
    }

    reloadMessage() {
        loadMessage();
        this.msg = document.getElementById("result");
        this.msgN = this.msg.children[1].children[0].children[1].children[0].innerText;
    }

    getMessage(n) {
        return this.msg.children[1].children[0].children[this.msgN - n].children[2].innerHTML;
    }

    getUser(n) {
        let ret = this.msg.children[1].children[0].children[this.msgN - n].children[1].innerText;
        ret = ret.replace(/([\s\S]*?)@([0-9a-zA-Z]{7}?)([\s\S]*?)/, "$2");
        ret = ret.replace(this.getUserAdd(n), "");
        if (ret == "") { ret == "admin"; }
        return ret;
    }
    
    getUserName(n) {
        let ret = this.msg.children[1].children[0].children[this.msgN - n].children[1].innerText;
        ret = ret.replace(/([\s\S]*?)@([0-9a-zA-Z]{7}?)([\s\S]*?)/, "$1");
        ret = ret.replace(this.getUserAdd(n), "");
        if (this.getUser(n) == "admin") { ret = this.msg.children[1].children[0].children[this.msgN - n].children[1].innerText; }
        return ret;
    }
    
    getUserAdd(n) {
        let ret = this.msg.children[1].children[0].children[this.msgN - n].children[1].innerText;
        ret = ret.replace(/([\s\S]*?)@([0-9a-zA-Z]{7}?)([\s\S]*?)/, "$3");
        return ret;
    }

    setName(name) {
        this.name = name;
    }

    setSeed(seed) {
        this.seed = seed;
    }
}